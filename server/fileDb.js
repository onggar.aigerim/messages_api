const fs = require("fs");

const fileName = "./db.json";
let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(fileName);
      data = JSON.parse(fileContents);
    } catch(e) {
      data = [];
    }
  },
  getItems(date) {
    if (date) {
        const newData = data.sort((a, b) => {
          return new Date(a.datetime).toISOString() - new Date(b.datetime).toISOString();
        });
        const lastData = newData.find(item => {
          return new Date(item.datetime).toISOString() > new Date(date).toISOString();
        });
        return lastData;
    } else {
      if (data.length > 30) {
        const newData = data.slice(Math.max(data.length - 30, 1));
        return newData;
      } else {
        return data;
      }
    }
  },
  getItem(id) {
    const items = this.getItems();
    const item = items.find((i) => i.id === id);
    return item;
  },
  addItem(item) {
    data.push(item);
    this.save();
  },
  save() {
    fs.writeFileSync(fileName, JSON.stringify(data));
  }
};