const express = require("express");
const cors = require("cors");
const messages = require("./routes/messages")
const fileDb = require("./fileDb");
const app = express();

const PORT = 8080;

fileDb.init();

app.use(cors());
app.use(express.json());
app.use("/messages", messages(fileDb));

app.listen(PORT, () => {
  console.log(
    `Server started at http://localhost:${PORT}`
  );
});