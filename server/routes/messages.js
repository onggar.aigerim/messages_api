const express = require("express");
const {nanoid} = require("nanoid");
const router = express.Router();

const createRouter = (db) => {
  router.get("/", (req, res) => {
    if (!req.query.datetime) {
      return res.send(db.getItems());
    }
    const date = new Date(req.query.datetime);
    if (isNaN(date.getDate())) res.status(400).send({"error": "Invalid datetime"});
    return res.send(db.getItems(req.query.datetime));
  });
  router.get("/:id", (req, res) => {
    const message = db.getItem(req.params.id);
    if (!message) {
      res.sendStatus(404);
    }
    res.send(message);
  });
  router.post("/", (req, res) => {
    const body = {...req.body, id: nanoid(), datetime: new Date()};
    if (body.message === undefined || body.author === undefined) {
      res.status(400).send({"error": "Author and message must be present in the request"});
    } else {
      db.addItem(body);
      res.send(body);
    }
  });
  return router;
}

module.exports = createRouter;