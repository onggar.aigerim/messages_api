import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import ChatMessages from "../../components/ChatMessages/ChatMessages";
import NewMessage from "../../components/NewMessage/NewMessage";
import { addMessage, fetchMessages } from "../../store/actions/messagesActions";

const Div = styled.div`
  width: 303px;
  padding: 45px 15px;
  height: 600px;
  border-radius: 10px;
  margin: 20px auto;
  background: url("../../.././img/phone_p.png") no-repeat 50%;
  background-size: cover;
`;

const Chat = () => {
  const dispatch = useDispatch();
  const messages = useSelector(state => state.messages.messages);
  const lastDatetime = useSelector(state => state.messages.lastDatetime);
  
  const [values, setValues] = useState({
    message: "",
    author: ""
  });

  useEffect(() => {
    const getDatetime = () => {
      if (lastDatetime) {
        return '?datetime=' + lastDatetime;
      }
      return '';
    };

    const getMessages = setInterval(() => {
      dispatch(fetchMessages(getDatetime()))
    }, 2000);

    return () => {
      clearInterval(getMessages)
    };
    
  }, [dispatch, lastDatetime]);

  const onChangeVal = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value
    });
  };

  const postRequest = async () => {
    const message = {
      message: values.message,
      author: values.author
    };
    dispatch(addMessage(message));
  };

  const addNewMessage = (e) => {
    e.preventDefault();
    if (values.message && values.author) {
      postRequest();
      setValues({
        message: "",
        author: ""
      });
    };
  };

  return (
    <Div>
      <ChatMessages 
        messages={messages}
      />
      <NewMessage 
        submitHandler={(e) => addNewMessage(e)}
        inputValue={values.author}
        textValue={values.message}
        changeHandler={(e) => onChangeVal(e)}
      />
    </Div>
  )
};

export default Chat;