import ReactDOM from 'react-dom/client';
import { configureStore } from "@reduxjs/toolkit";
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import "./index.css";

import App from './App';
import messagesReducer from './store/reducers/messagesReducer';

const store = configureStore({reducer: {
  messages: messagesReducer
}});

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(app);
