import axios from "../../axiosMessages";
import { ADD_MESSAGE_SUCCESS, FETCH_MESSAGES_SUCCESS, LAST_MESSAGE_SUCCESS } from "../actionsTypes";

const fetchMessagesSuccess = (messages) => {
  return {type: FETCH_MESSAGES_SUCCESS, messages};
};

const getLastMessages = (messageDate) => {
  return {type: LAST_MESSAGE_SUCCESS, messageDate};
};

export const fetchMessages = (datatime) => {
  return async dispatch => {
    try {
      const response = await axios.get("/messages" + datatime);
      if (response.data) {
        if (response.data.length) {
          const lastMessage = response.data[response.data.length - 1]; 
          dispatch(getLastMessages(lastMessage.datetime));
          dispatch(fetchMessagesSuccess(response.data));
        } else {
          dispatch(getLastMessages(response.data.datetime));
          dispatch(fetchMessagesSuccess(response.data));
        }
      } else {
        dispatch(fetchMessagesSuccess([]));
      }
    } catch (e) {
      console.log(e);
    }
  };
};

const addMessageSuccess = () => {
  return {type: ADD_MESSAGE_SUCCESS};
};

export const addMessage = (messageData) => {
  return async dispatch => {
    try {
      await axios.post("/messages", messageData);
      dispatch(addMessageSuccess());
    } catch (e) {
      console.log(e);
    }
  };
};