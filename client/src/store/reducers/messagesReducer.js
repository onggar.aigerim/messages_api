import { FETCH_MESSAGES_SUCCESS, LAST_MESSAGE_SUCCESS } from "../actionsTypes";

const initialState = {
  messages: [],
  lastDatetime: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES_SUCCESS:
      return {...state, messages: [...state.messages].concat(action.messages)}
    case LAST_MESSAGE_SUCCESS:
      return {...state, lastDatetime: action.messageDate}
    default:
      return state;
  }
};

export default reducer;