import styled from "styled-components";

const Div = styled.div`
  border: 1px solid;
  margin: 10px;
  padding: 5px;
  border-radius: 20px 20px 20px 0;
  background: #04e37426;
  & p {
    font-size: 18px;
  }
`;

const Message = ({
  author, 
  message, 
  date
}) => {
  return (
    <Div>
      <p><strong>Author:</strong> {author}</p>
      <p><strong>Date:</strong> {date}</p>
      <p><strong>Message:</strong> {message}</p>
    </Div>
  )
};

export default Message;