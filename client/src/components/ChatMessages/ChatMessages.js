import styled from "styled-components";
import Message from "../Message/Message";

const Div = styled.div`
  height: 470px;
  overflow: scroll;
  border: 1px solid #ccc;
  margin: 3px;
`;

const ChatMessages = ({messages}) => {
  return (
    <Div>
      {messages.map((message) => {
        const date = new Intl.DateTimeFormat("ru", {
          year: 'numeric', month: 'numeric', day: 'numeric',
          hour: 'numeric', minute: 'numeric',
          hour12: false
        }).format(new Date(message.datetime));

        return <Message 
          key={message.id} 
          author={message.author} 
          message={message.message} 
          date={date} 
        />
      })}
    </Div>  
  )
};

export default ChatMessages;