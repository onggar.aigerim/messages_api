import styled from "styled-components";

const Form = styled.form`
  display: grid;
  grid-template-areas: 'textarea textarea' 'input button';
  align-items: center;
  justify-content: center;
  grid-column-gap: 5px;
  grid-row-gap: 5px;
  margin: 10px;
  & input, textarea {
    font-size: 18px;
    font-family: 'Courier New', Courier, monospace;
    padding: 6px;
  }
  & input {
    grid-area: input;
  }
  & textarea {
    max-width: 280px;
    height: 70px;
    grid-area: textarea;
  }
  & button {
    background: #dde6e3;
    border: 1px solid;
    font-size: 18px;
    padding: 6px;
    border-radius: 10px;
    grid-area: button;
  }
`;

const NewMessage = ({
  submitHandler,
  inputValue,
  textValue,
  changeHandler
}) => {
  return (
    <Form onSubmit={submitHandler}>
      <textarea 
        name="message" 
        value={textValue} 
        onChange={changeHandler}
        placeholder="Message"
      />
      <input 
        name="author" 
        value={inputValue} 
        onChange={changeHandler}
        placeholder="Author"
      />
      <button>Send</button>
    </Form>
  )
};

export default NewMessage;